# ACA Summary

This repository contains all the latex files for my summary
of the lecture "Angewandte Computer Architektur", 
written for the course in fall 2017. The lecture was held
by Anton Gunzinger. The summary is based mostly on the
lecture slides, especially the images were extracted from them.

