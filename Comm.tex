\section{Communication}

Communication can be done with standard LAN (Local Area Network), e.g. Ethernet
or high-speed SAM (System Area Network), e.g. T-Net.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.6\textwidth,natwidth=610,natheight=642]{Imgs/CommNet}
	\caption{Commodity Supercomputer Architecture}
	\label{fig:bca}
\end{figure}

Basic properties of a high-speed communication network (SAN) include the
following: Communication bandwidth and latency, bisectional bandwidth,
the design, implementation and production cost, the scalability, the 
possible topologies.

\subsection{Latency}

Assume the amount of data (packet size) is $b$, the maximum bandwidth is $B_{0}$
and the latency is $t_{latency}$. We have get for
the communication time: $t_{com}(b) = t_{latency} + \frac{b}{B_{0}}$.
And finally the actual bandwidth $B'(b)$ as shown in Figure \ref{fig:bwps}.
$b_{2}$ is the packet size at which the actual bandwidth is half of its maximum.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth,natwidth=610,natheight=642]{Imgs/BandW}
	\caption{Bandwidth Depending on Packet Size}
	\label{fig:bwps}
\end{figure}

\subsection{Bisectional Bandwidth}

The bisectional bandwidth is the minimum bandwidth between two halves of the network.
It is often expressed as bandwidth per PE.

\subsection{Switches}

Figure \ref{fig:swit} shows two different switches, a uni-directional and a bi-directional
one.

\begin{figure}[ht]
	\centering
	\begin{minipage}{.53\textwidth}
		\centering
		\includegraphics[width=0.85\textwidth,natwidth=610,natheight=642]{Imgs/UniSwitch}
	\end{minipage}%
	\begin{minipage}{.47\textwidth}
		\centering
		\includegraphics[width=0.85\textwidth,natwidth=610,natheight=642]{Imgs/BiSwitch}
	\end{minipage}
	\caption{A Uni-Directional 2-Way and a Bi-Directional 12-Way Switch}
	\label{fig:swit}
\end{figure}

\subsection{Routing}

Data that needs to be transported through the network is divided into packets.
The packet consists of a header with network and routing information, checking information
for the packet and the data itself. Switches receive, handle and forward the packets.

\textbf{Source Based Routing}: 
The packet source tells the packet how to find the destination. The switch
tears off his routing information and reads the instructions.

\textbf{Destination Based Routing}:
The packet contains an ID and the switch finds the destination. The switch
contains a routing table and sends it to the ports associated with this ID
from the packet. The packet is untouched.

\begin{center}
	\begin{tabular}{ | p{0.4 \textwidth} | l |}
		\hline
		\textbf{Source Based Routing} & \textbf{Destination Based Routing}  \\ 
		\hline
		\begin{minipage}{.4\textwidth}
			The packet source tells the packet how to find the destination. The switch
			tears off his routing information and reads the instructions.
		\end{minipage}%
		 & 
		\begin{minipage}{.5\textwidth}
			The packet contains an ID and the switch finds the destination. The switch
			contains a routing table and sends it to the ports associated with this ID
			from the packet. The packet is untouched.
		\end{minipage}
		\\ 
		\hline
		\begin{minipage}{.4\textwidth}
			\textbf{+} Simple switch hardware\\
			\textbf{+} Simple routing\\
			\textbf{+} No configuration necessary\\
			\textbf{-} No broad- and multicasts\\
			\textbf{-} No adaptive routing\\
			\textbf{-} Source nodes must know the
						topology and the distribution of
						the nodes\\
			\textbf{-} Faults force node reconfiguration\\
		\end{minipage}%
		 & 
		\begin{minipage}{.5\textwidth}
			\textbf{+} Broad- and multicasts possible\\
			\textbf{+} Adaptive routing possible\\
			\textbf{+} Nodes must not know anything
						about the topology\\
			\textbf{+} Faults are handled by switch
						management only\\
			\textbf{+} No processing on source node\\
			\textbf{-} Complex hardware with perswitch
						routing table\\
			\textbf{-} Per-switch configuration\\
		\end{minipage}\\
		\hline
	\end{tabular}
\end{center}

\subsection{Switching}

\subsubsection{Store and Forward Switching}

In this setting, the switch waits until the whole packet arrives
and checks if it is valid and then forwards it to the next switch.
This prevents invalid packets from using the bandwidth.

\subsubsection{Cut Through Switching}

In this case the switch starts forwarding the packet as soon
as it knows where it needs to go. This leads to faster propagation
of a packet through the network, but the invalid packets get forwarded
as well.

\subsubsection{Wormhole Switching}

This behaviour is quite similar to cut-through switching, 
commonly called "virtual cut-through," 
the major difference being that cut-through flow 
control allocates buffers and channel 
bandwidth on a packet level, 
while wormhole flow control does this on the flit level.


\subsection{Topologies}

\begin{center}
	\begin{tabular}{ | p{0.6 \textwidth} | l |}
		\hline
		\textbf{$d$-Dimensional Mesh} &   \\ 
		\hline
		\begin{minipage}{.6\textwidth}
			\textbf{+} Short physical distances\\
			\textbf{+} No size limits\\
			\textbf{+} Latency scales with $d$-th root of the network
						size\\
			\textbf{+} SAN cost scales with size\\
			\textbf{+} Multiple paths to the destination\\
			\textbf{+} Size is increased in small steps\\
			\textbf{-} Bandwidth scales with $d$-th root of the
						network size if $d>1$, if $d=1$ id does not scale
						at all\\
			\textbf{-} Multicasts make heavy processing necessary\\
			\textbf{-} Deadlock prevention necessary\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			$2$-dimensional example mesh.\\
			\includegraphics[width=0.7\textwidth,natwidth=610,natheight=642]{Imgs/2DMesh}
		\end{minipage}
		\\ 
		\hline
		\hline
		\textbf{$d$-Dimensional Torus} &   \\ 
		\hline
		\begin{minipage}{.6\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Latency scales with $d$-th root of the
						network size\\
			\textbf{+} SAN cost scales with size\\
			\textbf{+} Multiple paths to the destination\\
			\textbf{+} Size is increased in small steps\\
			\textbf{-} Bandwidth scales with $d$-th root of the
						network size if $d>1$, if $d=1$ it does not scale
						at all\\
			\textbf{-} Multicasts make heavy processing
						necessary\\
			\textbf{-} Deadlock prevention necessary\\
			\textbf{-} Long cables for wrap-around links
						necessary\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			$2$-dimensional example torus. The figure at the bottom shows 
			how the wiring is done.\\
			\includegraphics[width=0.48\textwidth,natwidth=610,natheight=642]{Imgs/2DTorus}
			\includegraphics[width=0.48\textwidth,natwidth=610,natheight=642]{Imgs/2DTorusWir}
		\end{minipage}
		\\ 
		\hline
		\hline
		\textbf{$k$-Ring} &   \\ 
		\hline
		\begin{minipage}{.6\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Latency scales with size\\
			\textbf{+} SAN cost scales with size\\
			\textbf{+} Multiple paths to the destination\\
			\textbf{+} Size is increased in small steps\\
			\textbf{-} Bandwidth scales with $d$-th root of the
						network size if $d>1$, if $d=1$ it does not scale
						at all\\
			\textbf{-} Multicasts make heavy processing
						necessary\\
			\textbf{-} Deadlock prevention necessary\\
			\textbf{-} Long cables for wrap-around and internal links
						necessary\\
			\textbf{-} Monolithic, hard to partition\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			A $2$-ring of size $8$, $a_{1} = 1, a_{2} = 3$\\
			\includegraphics[width=0.7\textwidth,natwidth=610,natheight=642]{Imgs/2D2Torus}
			In general a $k$-ring of size $N$ is a graph with nodes placed in a ring
			where each node is connected to its $a_{1}$-th, $a_{2}$-th, ... , $a_{k}$-th
			neighbor. The degree of reach node is $2k$, usually $a_{1} = 1$.
		\end{minipage}
		\\ 
		\hline
	\end{tabular}
\end{center}


\begin{center}
	\begin{tabular}{ | p{0.5 \textwidth} | l |}
		\hline
		\textbf{Tree} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Latency scales with size\\
			\textbf{+} No deadlocks\\
			\textbf{+} Fast and simple broadcasts\\
			\textbf{-} Bandwidth does not scale with size\\
			\textbf{-} Single point of failure\\
			\textbf{-} SAN cost does not scale with size\\
			\textbf{-} Long distances for connections at the
						tree root\\
			\textbf{-} Size only increased in large steps
						(factor of fan-out) for balanced
						systems\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			A $4$-ary tree of height $2$.\\
			\includegraphics[width=0.9\textwidth,natwidth=610,natheight=642]{Imgs/Tree}
		\end{minipage}
		\\ 
		\hline
		\hline
		\textbf{Fat Tree} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Latency scales with size\\
			\textbf{+} No deadlocks\\
			\textbf{+} Bandwidth scales with size\\
			\textbf{+} Multiple paths to the destination\\
			\textbf{+} Fast and simple broadcasts\\
			\textbf{-} SAN cost does not scale with size\\
			\textbf{-} Long distances for connections at the
						tree root\\
			\textbf{-} Size only increased in large steps
						(factor of fan-out) for balanced
						systems\\
			\textbf{-} Expensive\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			$L=4$ Down/$4$ Up fat tree with $k=3$ layers. Note that the processors 
			are not shown in the figure, they are at the outgoing connections on top.\\
			\includegraphics[width=\textwidth,natwidth=610,natheight=642]{Imgs/FatTree}
			Let $L$ be the switch size and $k$ the number of layers, then we
			need $k^{L}$ processors, $L\cdot k^{L-1}$ switches and $L\cdot k^{L}$ cables.
		\end{minipage}
		\\ 
		\hline
		\hline
		\textbf{Fully Connected} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} Constant, low latency\\
			\textbf{+} Bandwidth scales with size\\
			\textbf{+} SAN cost scales with size\\
			\textbf{+} Multiple paths to the destination\\
			\textbf{+} Size is increased in small steps\\
			\textbf{+} Fast and simple broadcasts\\
			\textbf{+} No deadlocks\\
			\textbf{-} Limited network size\\
			\textbf{-} Trade-off between number of nodes and
						network size\\
			\textbf{-} Very expensive\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			Size $8$ fully connected example network:\\
			\includegraphics[width=0.8\textwidth,natwidth=610,natheight=642]{Imgs/FullCon}
		\end{minipage}
		\\ 
		\hline
	\end{tabular}
\end{center}


\begin{center}
	\begin{tabular}{ | p{0.5 \textwidth} | l |}
		\hline
		\textbf{$d$-Dimensional Hypercube} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Latency scales with size\\
			\textbf{+} Bandwidth scales with size\\
			\textbf{+} SAN cost scales with size\\
			\textbf{+} Multiple paths to the destination\\
			\textbf{+} Simple and fast broadcasts\\
			\textbf{-} Bandwidth does not scale with size\\
			\textbf{-} Deadlock prevention necessary\\
			\textbf{-} SAN cost does not scale with size\\
			\textbf{-} Long cables for links necessary\\
			\textbf{-} Size only increased in large steps\\
			\textbf{-} Trade-off between number of nodes
						and network size\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			Hyper cubes of dimensions $0$ to $4$.\\
			\includegraphics[width=0.9\textwidth,natwidth=610,natheight=642]{Imgs/HyperC}
		\end{minipage}
		\\ 
		\hline
		\hline
		\textbf{Bus} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} Low cost\\
			\textbf{+} Fast and stable latency\\
			\textbf{+} No deadlocks\\
			\textbf{+} Size is increased in small steps\\
			\textbf{+} Fast and simple broadcasts\\
			\textbf{-} Bandwidth does not scale with size\\
			\textbf{-} Limited to short interconnects\\
			\textbf{-} Bus is single point of failure\\
			\textbf{-} Limited size\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			A bus network connecting $4$ processors.\\
			\includegraphics[width=0.9\textwidth,natwidth=610,natheight=642]{Imgs/Bus}
		\end{minipage}
		\\ 
		\hline
		\hline
		\textbf{Multistage Networks} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Bandwidth scales with size\\
			\textbf{+} Latency scales with size\\
			\textbf{+} Fast and simple broadcasts\\
			\textbf{+} No deadlocks\\
			\textbf{-} SAN cost does not scale with size\\
			\textbf{-} Long distances for connections\\
			\textbf{-} Every switch is a point of failure\\
			\textbf{-} Size only increased in large steps (factor
						of fan-out) for balanced systems\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			Multistage networks with $L=1$ to $4$ layers and switch size $k=2$:\\
			\includegraphics[width=\textwidth,natwidth=610,natheight=642]{Imgs/MultiSt}
			Parameters similar to fat tree, except the number of cables is:\\
			$(L+1) k^{L}$.
		\end{minipage}
		\\ 
		\hline
	\end{tabular}
\end{center}

\begin{center}
	\begin{tabular}{ | p{0.5 \textwidth} | l |}
		\hline
		\textbf{Omega/Shuffle Network} &   \\ 
		\hline
		\begin{minipage}{.5\textwidth}
			\textbf{+} No size limits\\
			\textbf{+} Bandwidth scales with size\\
			\textbf{+} Latency scales with size\\
			\textbf{+} Fast and simple broadcasts\\
			\textbf{+} No deadlocks\\
			\textbf{-} SAN cost does not scale with size\\
			\textbf{-} Long distances for connections\\
			\textbf{-} Every switch is a point of failure\\
			\textbf{-} Size only increased in large steps (factor
						of fan-out) for balanced systems\\
		\end{minipage}%
		 & 
		\begin{minipage}{.4\textwidth}
			Omega/Shuffle network with $4$ layers and switch size $2$:\\
			\includegraphics[width=0.9\textwidth,natwidth=610,natheight=642]{Imgs/OmegaSh}
			Parameters are the same as for multistage networks.
		\end{minipage}
		\\ 
		\hline
	\end{tabular}
\end{center}




















