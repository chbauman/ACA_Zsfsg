


all:
	pdflatex summ.tex
	biber summ
	pdflatex summ.tex
	pdflatex summ.tex

clean: 
	rm -f *.bbl
	rm -f *.bcf
	rm -f *.blg
	rm -f *.xml
	rm -f *.log
	rm -f *.pdf
	rm -f *.aux
	rm -f *.out
