\section{Parallel Processing}

Used in most supercomputers. Parallel processing reduces cost, 
brings better performance, smaller size, smaller power usage, 
simpler programming models, modularity and scalability.
Parallel commodity supercomputer use commodity components
and CMOS chip technology in a modular way, this makes them 
less expensive compared to conventional vector or parallel supercomputers
which use specialized components. The most relevant parameters of 
a parallel system are performance, cost, software maintenance, 
applications, scalability, communication system, power consumption 
and maintenance.
The structure of a parallel machine looks as:

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth,natwidth=610,natheight=642]{Imgs/ParMach}
	\caption{Parallel Machine Architecture}
	\label{fig:pma}
\end{figure}

\subsection{Classification of Parallel Machines According to Gordon Bell}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth,natwidth=610,natheight=642]{Imgs/ClassMach}
	\caption{Parallel Machine Classification}
	\label{fig:pmc}
\end{figure}

\subsection{Multiprocessor with Centralized Memory}

SMP: Symmetrical Multiprocessing, UMA: Uniform Memory Access.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth,natwidth=610,natheight=642]{Imgs/CentMem}
	%~ \caption{Parallel Machine Classification}
	%~ \label{fig:pmc}
\end{figure}


\subsection{Multiprocessor with Distributed Memory}

NUMA: Non-Uniform Memory Access, CC-NUMA: Cache-Coherent NUMA.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth,natwidth=610,natheight=642]{Imgs/DistMem}
	%~ \caption{Parallel Machine Classification}
	%~ \label{fig:pmc}
\end{figure}

\subsection{Multiprocessor with Fixed Network}

Same as for the one with distributed memory, except that the Communication Network
is interchanged with a Fixed Communication Network (SAN), SAN: System Area Network.

\subsection{Multiprocessor with Scalable Network}

Same as for the one with distributed memory, except that the Communication Network
is interchanged with a Scalable Communication Network (SAN).

\subsection{Parallel Processing Speed-Up and Efficiency}

The optimal speed-up with $p$ processors is $p$. Absolute performance is often forgotten.
For the following reasons the optimal speed-up is often not achieved: 
\begin{itemize}
	\item Serial part of the code (Amdahl's Law)
	\item Communication part (amount of data being transferred)
	\item Synchronization and co-ordination
	\item Distribution of the computation (balance of the job)
\end{itemize}
Hyper speed-up is when the speed-up is better that optimal, this
happens when the smaller problem size using more processors allows
the data to be higher up in the memory hierarchy.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.4\textwidth,natwidth=610,natheight=642]{Imgs/SpeedUp}
	\caption{Different Achieved Speed-Ups}
	%~ \label{fig:pmc}
\end{figure}

Let $p$ be the number of processors, $S(p)$ the speed-up, $t_{e}(p)$ the execution time and 
$E(p)$ the efficiency always using $p$ processors, we have
\begin{equation}
	S(p) = \frac{t_{e}(1)}{t_{e}(p)}, E(p) = \frac{S(p)}{p}
\end{equation}
Further let $t_{s}$ be the time used for the serial part, $t_{p}$ the time used
for the parallel part and $t_{c}$ the time used for the communication part.
So we have $t_{e}(p) = t_{s} + t_{p} / p + p \cdot t_{c}$ for $p > 1$, $t_{e}(1) = t_{s} + t_{p}$ and
\begin{equation}
	S(p) = \frac{t_{e}(1)}{t_{e}(p)} = \frac{t_{s} + t_{p}}{t_{s} + t_{p} / p + p \cdot t_{c}} = \frac{1}{k_{s} + k_{p} / p + p \cdot k_{c}} 
\end{equation}
where $k_{s} = \frac{t_{s}}{t_{e}(1)}$ is the fraction of the code that
is serial, $k_{p} = \frac{t_{p}}{t_{e}(1)}$ is the fraction of the code that 
is parallel and $k_{c} = \frac{t_{c}}{t_{e}(1)}$. Amdahl's Law considers the
case where $k_{c} = 0$, we have that $\lim\limits_{p \rightarrow \infty} S(p) = 0$ if $k_{c} > 0$
and  $\lim\limits_{p \rightarrow \infty} S(p) = \frac{1}{k_{s}}$ if $k_{c} = 0$ (Amdahl).
Given $k_{c} > 0$, $S(p)$ attains its maximum  $p = \sqrt{\frac{k_{p}}{k_{c}}}= \sqrt{\frac{t_{p}}{t_{c}}}$,
else at $p \rightarrow \infty$ ($k_{c} = 0$).

\begin{figure}
	\centering
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=0.95\textwidth,natwidth=610,natheight=642]{Imgs/SpeedUp2}
		\label{fig:test1}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=0.95\textwidth,natwidth=610,natheight=642]{Imgs/Effic}
		\label{fig:test2}
	\end{minipage}
	\caption{Speed-Up and Efficiency for different $k_{s}$ and $k_{c}$}
\end{figure}

\subsection{Performance}

Let $p$ be the number of processors and $P(p)$ be the performance that
is achieved using $p$ processors. We have:
\begin{equation}
	P(p) = \frac{\# \text{FP instructions}}{t_{e}(p)} = S(p) \cdot P(1) 
	= \frac{s_{0} + c_{0} n_{0}}{(s_{0} + c_{0} n_{0})CPI \cdot t_{0}} = \frac{1}{CPI \cdot t_{0}}
\end{equation}
where $s_{0} = \#$ serial instructions, $n_{0} = \#$ parallel tasks, 
$c_{0} = \#$ instructions per task, $CPI = \#$ clocks per instruction
and $t_{0} =$ processor cycle time. Further $(s_{0} + c_{0} n_{0}) = $ total number
of instructions.

\subsection{Communication}

A simple communication model for all-to-all communication to illustrate
the limits that the bandwidth poses can be seen in Figure \ref{fig:scm}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth,natwidth=610,natheight=642]{Imgs/ParComm}
	\caption{Simple Communication Model}
	\label{fig:scm}
\end{figure}

The calculation performance in parallel systems is often limited
by the communication bandwidth.
Let $b_{0} = \#$ data per task and $B =$ network bandwidth. We have:
\begin{equation}
	t_{com} = \frac{\text{Amount of Data}}{\text{Bandwidth}} = \frac{n_{0} b_{0}}{B}\text{ and } t_{calc} = \frac{n_{0}c_{0}\cdot CPI\cdot c_{0}}{p}
\end{equation}
Let $p_{max}$ be the maximum number of processors that should be used. Ideally
the $t_{com} \approx t_{calc}$. If we solve the equation $t_{com} = t_{calc}$
we get:
\begin{equation}
	p_{max} = \frac{B\cdot c_{0} \cdot CPI \cdot t_{0}}{b_{0}} = \frac{t_{e}(1)}{t_{com}}
\end{equation}
%~ $P_{max} = P(p_{max})=  P(1) \cdot p_{max}= \frac{B \cdot c_{0}}{b_{0}}$% This is a bit strange
%~ where $P_{max}$ denotes the maximum performance. 
Let further $t_{tot} = t_{com} + t_{calc}$, then we get for the speed-up.
\begin{equation}
	S(p) = \frac{1}{\frac{1}{p} + \frac{b_{0}}{B \cdot c_{0} \cdot CPI \cdot t_{0}}} = \frac{1}{\frac{1}{p} + \frac{1}{p_{max}}}
\end{equation}
It follows that $\lim\limits_{p \rightarrow \infty} S(p) = p_{max} = \frac{t_{com}}{t_{e}(1)}$, which
is illustrated in Figure \ref{fig:sulfc}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth,natwidth=610,natheight=642]{Imgs/CommLim}
	\caption{Speed-Up Limits from Communication}
	\label{fig:sulfc}
\end{figure}

\subsection{Load Balancing}

If the number of parallel tasks $n_{0}$ is close or even smaller than the number
of processors $p$, we won't get the optimal speed-up, we have:
\begin{equation}
	S(p) = \frac{1}{k_{s} + k_{p} / n_{0} \cdot \lceil n_{0}/ p \rceil + p \cdot k_{c}} 
\end{equation}
since we can't assign a fraction of a task to a processor.















